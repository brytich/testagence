import VehiculeIndex from "./domains/Vehicles/VehiculePage";
// review : ne pas lancer le projet via un terminal WSL linux car cela prend trop de temps de chargement ( 1min )


function App() {
  return (
    <div className="App">
        <VehiculeIndex />
    </div>
  );
}

export default App;
