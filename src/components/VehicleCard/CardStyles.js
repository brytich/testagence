import styled from "styled-components";

export const CardContainer = styled.div`
display: grid;
padding: 15px;
margin:15px;
grid-template-columns: 300px;
grid-template-rows: 15px 40px 0px;
background: radial-gradient(circle, rgba(62,63,70,1) 49%, rgba(89,85,86,1) 100%);
border-radius: 18px;
box-shadow: 5px 5px 15px rgba(0, 0, 0, 0.9);
text-align: center;`;

export const CardMiles = styled.div`
color: white;
font-size: 0.8rem;
font-weight: 300;`;

export const CardText = styled.p`
color: white;
font-size: 0.8rem;
font-weight: 300;`;

export const CardId = styled.span`
color: rgb(255, 7, 110);
font-size: 0.8rem;`;

export const CardMarkTitle = styled.h1`
color: white;
margin-top: 0px;
font-size: 2rem;
box-sizing: border-box;
min-width: 0px;
line-height: 1.2;
margin: 0px; `;

export const CardColor = styled.div`  
color: white;
font-size: 0.8rem;
font-weight: 300;`;

export const CardDoor = styled.div`  
color: white;
font-size: 0.8rem;
font-weight: 300;`;



