import {
  CardMiles,
  CardText,
  CardId,
  CardMarkTitle,
  CardColor,
  CardDoor,
  CardContainer
} from "./CardStyles";

// Contient les infos que va afficher la Card
// fix : ici j'ai redefini les balises <div> en import depuis CardStyles les styled component afin de mieux s'y retrouver , c'est plus lisible
// review : Je prefere utiliser un const plutot qu'un let
const VehicleCard = (props) => {
  return (
    <CardContainer>
      <CardId>
        #{props.id}
      </CardId>
      <CardMarkTitle>
        {props.make_and_model}
      </CardMarkTitle>
      <CardColor>
        Color car : {props.color}
      </CardColor>
      <CardText>
        Fuel : {props.fuel_type}
      </CardText>
      {props.doors < 2 && <CardDoor>{props.doors} Door</CardDoor>}
      {props.doors >= 2 && <CardDoor>{props.doors} Doors</CardDoor>}
      <CardMiles>
        {props.mileage} Miles
      </CardMiles>
    </CardContainer>
  );
};

export default VehicleCard;
