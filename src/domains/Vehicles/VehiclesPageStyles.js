import styled from "styled-components";

export const CardContainer = styled.div`
display: flex;
flex-wrap: wrap;
justify-content: space-between;
`;

export const PaginationButton = styled.div `
display: flex;
padding: 20px;
width: 100%;
margin: 25px;
text-align: center;
justify-content: center;
`;

export const MainPage = styled.div `
margin: auto 10px;
width: auto;
`;