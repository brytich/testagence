import React, { useEffect, useState } from "react";
import VehicleCard from "../../components/VehicleCard/VehicleCard";
import { getVehicles } from "../../services/vehicles";
import {CardContainer,
        PaginationButton,
        MainPage } from './VehiclesPageStyles';

// review : concernant les fihciers et dossiers j'aurai tendance à choisir des noms plus adequats afin de mieux s'y retrouver , et renommer les fichiers JS pour mettre des Majuscules en premiere lettre
// fix : J'ai supp le bout de code car il n'avait pas d 'utilité
// review : en ce qui concerne l 'architecture du code , je prefere ranger les fichiers dans les dossiers spécifique ( example : dossier component => dossier Vehicules => fichier vehiculeCard / VehiculePage  )
// review :                                                                                                                                                           => dossier Styles => CardStyles / VehiculePageStyle

  const VehiculeIndex = () => {
  const [cars, setCars] = useState([]);
  const [number, setNumber] = useState(1);
  // nombre de page en fonction du nombre de voiture appelé depuis l api
  const [carPerPage] = useState(10);

//appel de l'API 
  useEffect(() => {
    const fetchCars = async () => {
      setCars(await getVehicles());
    };
    fetchCars();
  }, []);

  
  const lastCar = number * carPerPage;
  const firstCar = lastCar - carPerPage;
  const currentCar = cars.slice(firstCar , lastCar);
  const pageNumber = [];
  
  for (let i = 1; i <= Math.ceil(cars.length / carPerPage); i++) {
  pageNumber.push(i);
  }
  
  const ChangePage = (pageNumber) => {
    setNumber(pageNumber);
  };

    return (
    <>
    <MainPage>
      <CardContainer>
        {currentCar.map((car) => (
          // le ... affichera la data appelé dans VehicleCard ( example :  make_and_model ou fuel_type)
          <VehicleCard key={car.id} {...car} />
        ))}   
        <PaginationButton>
            <button
              onClick={() => setNumber(number - 1)}
            >
              Précédent
            </button>

            {pageNumber.map((Page) => {
              return (
                <>
                  <button
                    onClick={() => ChangePage(Page)}
                  >
                    {Page}
                  </button>
                </>
              );
            })}
            <button
              onClick={() => setNumber(number + 1)}
            >
              Suivant
            </button>
          </PaginationButton>
      </CardContainer>
    </MainPage>
    </>
  );
};

export default VehiculeIndex;
