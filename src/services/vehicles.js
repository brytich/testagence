const getVehicles = async () => {
  // fix : j'ai changé la valeur de size afin de mettre en place la pagination
  const response = await fetch(
    "https://random-data-api.com/api/vehicle/random_vehicle?size=100"
  );
  const vehicles = await response.json();
  console.log("debug", vehicles);
  return vehicles;
};

export { getVehicles };

// review : Etant donné que c'est un petit projet , j'aurai peut etre placé ce petit bout de code dans le fichier qui donne le rendu mais c'est bien mieux rangé quand meme comme ca
// review : Dans le cas d'une API plus complexe, j'utilise plus particulierement Axios pour ses fonctionnalité plus simple à utiliser , après c'est chacun sa zone de confort 